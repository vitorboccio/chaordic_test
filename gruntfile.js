module.exports = function(grunt) {
    grunt.initConfig({
        compass: {
            dist: {
                options: {
                    config: 'config.rb'
                }
            }
        },
        uglify: {
            options: {
                report: 'gzip',
                preserveComments: false,
                beautify: false
            },
            dist: {
                files: {
                    "_assets/build/js/index.js": "_assets/src/js/index.js"
                }
            }
        },
        jshint: {
            ignore_warning: {
                options: {
                    '-W099': true,
                    /*'-W041': true,*/
                    '-W044': true,
                    '-W033': true,
                    '-W043': true,
                    '-W049': true,
                    '-W004': true,
                    '-W065': true,
                    '-W083': true,
                },
                src: ['_assets/src/js/*.js'],
            }
        },
        jslint: { // configure the task
            files: [
                '_assets/src/js/*.js'
            ],
            directives: {
                browser: true,
                devel: true,
                passfail: false,
                ass: true,
                bitwise: true,
                closure: true,
                continue: true,
                debug: true,
                eqeq: false,
                es5: true,
                evil: true,
                forin: true,
                newcap: true,
                nomen: true,
                plusplus: true,
                regexp: true,
                unparam: true,
                sloppy: true,
                stupid: true,
                sub: true,
                todo: true,
                vars: true,
                white: true,
                predef: [ // array of pre-defined globals
                    '$', 'head', 'jQuery', 'tinyMCE', 'tinymce', 'Modernizr', 'ActiveXObject', 'plupload', 'clickLinkTrack'
                ]
            },
            options: {
                log: '_logs/lint.log',
                errorsOnly: true, // only display errors
                failOnError: false, // defaults to true
                shebang: true // ignore shebang lines
            }
        },
        concurrent: {
            watch: ['compass','uglify']
        },

        watch : {
            compass: {
                files: ['_assets/src/scss/**','_assets/build/img/**'],
                tasks: ['compass']
            },
            dist : {
                files: ['_assets/src/js/**'],
                tasks: [ 'jshint', 'uglify' ]
            }
        }
    });

    // load tasks
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-jslint');
    grunt.loadNpmTasks('grunt-concurrent');


    // register tasks
    grunt.registerTask('default', ['compass', 'jshint', 'uglify']);
    grunt.registerTask('w', ['watch']);
};
