function X(unit) {
    
    //Produto visitado
    var output = "<ul class='reference'>";
    output+=    "<li class='product'>" +
                    "<a target='_blank' class='detailUrl' href='" + unit.data.reference.item.detailUrl + "' >" +
                        "<img src='http://" + unit.data.reference.item.imageName + "' />" +
                        "<div class='itemName'>" + unit.data.reference.item.name + "</div>" +
                        "<div class='price'> Por: <strong>" + unit.data.reference.item.price + "</strong></div>" +
                        "<div class='conditionsPayment'>" + unit.data.reference.item.productInfo.paymentConditions + " sem juros</div>" +
                    "</a>" +
                "</li>"
    output+= "</ul>";
    document.getElementById("visited").innerHTML=output;

    //Itens recomendados
    print = "<ul class='recommendations' id='slide'>";
    for (var i in unit.data.recommendation) {
        var old;
        if (unit.data.recommendation[i].oldPrice !== null) {
            old = "De: " + unit.data.recommendation[i].oldPrice;
        } else {
            old = "";
        }
        print+= "<li class='product'>" +
                    "<a target='_blank' class='detailUrl' href='" + unit.data.recommendation[i].detailUrl + "' >" +
                        "<img src='http://" + unit.data.recommendation[i].imageName + "' />" +
                        "<div class='itemName'>" + unit.data.recommendation[i].name + "</div>" +
                        "<div class='oldPrice'>" + old + "</div>" +
                        "<div class='price'> Por: <strong>" + unit.data.recommendation[i].price + "</strong></div>" +
                        "<div class='conditionsPayment'>" + unit.data.recommendation[i].productInfo.paymentConditions + " sem juros</div>" +
                    "</a>" +
                "</li>"
    }
    print+= "</ul>";
    document.getElementById("recommended").innerHTML=print;
}

var script = document.createElement('script');
script.src = 'http://roberval.chaordicsystems.com/challenge/challenge.json?callback=X'

document.getElementsByTagName('head')[0].appendChild(script);
